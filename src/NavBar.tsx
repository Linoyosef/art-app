import React from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import PaletteIcon from '@mui/icons-material/Palette';
import { useTheme } from './ThemeContext';
import Switch from '@mui/material/Switch';
import Grid from '@mui/material/Grid';

const NavBar = () => {
  const { darkMode, toggleDarkMode } = useTheme();
  
  return (

    <AppBar position="static" style={darkMode ? { backgroundColor: 'black'} : { backgroundColor: 'grey'} }>
      <Toolbar >
      <Grid container spacing={2}>
      <Grid item xs={2}>
      <PaletteIcon sx={{ display: { xs: 'none', md: 'flex' }, mr: 1 }} />
      </Grid>
      <Grid item xs={8}>
        <Typography variant="h6" component={Link} to="/" style={{ textDecoration: 'none', color: 'white' }}>
          Home
        </Typography>
        <Button color="inherit" component={Link} to="/about" style={{ textDecoration: 'none', marginLeft: '20px' }}>
          About
        </Button>
        <Button color="inherit" component={Link} to="/contact" style={{ textDecoration: 'none', marginLeft: '20px' }}>
          Contact
        </Button>
        </Grid>
        <Grid item xs={2}>
        <Switch
        checked={darkMode}
        onChange={toggleDarkMode}
        inputProps={{ 'aria-label': 'controlled' }}
      />
      </Grid>
      </Grid>
      </Toolbar>
  
    </AppBar>
  );
};
export default NavBar;